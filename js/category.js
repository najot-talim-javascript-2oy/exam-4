const categoriesRow=document.querySelector(".users-row"),categoryForm=document.querySelector(".categorys-form"),TeacherModal=document.querySelector("#TeacherModal"),groups=document.querySelector("#category_group"),modalBtn=document.querySelector(".modal-btn"),addBtn=document.querySelector("#add-btn"),searchInput=document.querySelector("#search"),selectt=document.getElementById("asc"),pagination=document.querySelector(".pagination"),fomrElements=categoryForm.elements;let selected=null,page=new URLSearchParams(location.search).get("page")||1;const LIMIT=3;let nameOrder,search="";function getCategoryCard({avatar:e,firstName:a,phoneNumber:t,email:l,createdAt:s,isMarried:n,groups:i,id:o}){return`
  <div class="Cards">
          <div class="card" data-tilt>
            <img src="${e}" alt="">
            <h2 class="countryName">Name: <span class="Ism">${a}</span></h2>
            <h2 class="regionName">Number: <span class="Ism">${t}</span></h2>
            <h2>Email: <span class="Ism">${l}</span></h2>
            <h2>Groups: <span class="Ism">${i.length>0?i:"N1"}</span></h2>
            <h2>Married: <span class="Ism">${n?"Yes":"No"}</span></h2>

            <div class="bt">
                <div class="profile-details edit" data-bs-toggle="modal" data-bs-target="#TeacherModal" id="add-button"  onclick="editCategory(${o})" >
                    <a href="#" class="button edit">Edit 📝</a>
                </div>
                 <div class="profile-details" onclick="deleteCategory(${o})">
                    <a href="#" class="button">Delete  🗑</a>
                </div>
            </div>
                <div class="profile-details a2">
                   <a href="student.html" onclick="saveId(${o})" class="button">See students ${o} 👳‍♀</a>
               </div>
  
          </div>
  </div>
  `}function getPagination(e){axiosInstance(e).then(e=>{let a=Math.ceil(e.data.length/3),t="";if(a>1){t=1==page?`<li class="page-item" onclick="getPage(${--page})" > <button class="page-link disabled" disabled>Previos</button>
            </li>`:`<li class="page-item" onclick="getPage(${--page})"> <button class="page-link">Previos</button>
           </li>`,page++;for(let l=1;l<=a;l++)t+=`<li class="page-item  ${l===page?"Active":""}">
          <button class="page-link" onclick="getPage(${l})">${l}</button>
          </li>`;page==a?t+=`<li class="page-item" onclick="getPage(${++page})" > <button class="page-link disabled" disabled>next</button>
           </li>`:t+=`<li class="page-item" onclick="getPage(${++page})"> <button class="page-link" >Next</button>
            </li>`,page--,pagination.innerHTML=t}}).catch(e=>{alert(e)})}function getCategories(){categoriesRow.innerHTML="..loading",axiosInstance(`teacher?page=${page}&limit=3&firstName=${search}&sortBy=firstName&order=${nameOrder}`).then(e=>{let a=e.data;axiosInstance(`teacher?firstName=${search}`).then(e=>{let a,t=document.querySelector(".pagination");a=Math.ceil(e.data.length/3);let l="";if(a>1){l=1==page?`<li class="page-item" onclick="getPage(${--page})" > <button class="page-link disabled" disabled>Previos</button>
            </li>`:`<li class="page-item" onclick="getPage(${--page})"> <button class="page-link">Previos</button>
           </li>`,page++;for(let s=1;s<=a;s++)l+=`<li class="page-item  ${s===page?"active":""}">
            <button class="page-link" onclick="getPage(${s})">${s}</button>
            </li>`;page==a?l+=`<li class="page-item" onclick="getPage(${++page})" > <button class="page-link disabled" disabled>next</button>
           </li>`:l+=`<li class="page-item" onclick="getPage(${++page})"> <button class="page-link" >Next</button>
            </li>`,page--,t.innerHTML=l}}),categoriesRow.innerHTML="",a.forEach(e=>{categoriesRow.innerHTML+=getCategoryCard(e)})}).catch(e=>{alert(e.response.data),categoriesRow.innerHTML=""})}function getPage(e){page=e,location.replace(`index.html?page=${e}`),getCategories()}async function editCategory(e){selected=e;let a=await axiosInstance(`teacher/${e}`);fomrElements.avatar.value=a.data.avatar,fomrElements.firstName.value=a.data.firstName,fomrElements.category_phone.value=a.data.phoneNumber,fomrElements.category_email.value=a.data.email,fomrElements.category_group.value=a.data.groups,modalBtn.innerHTML="Save Category"}async function deleteCategory(e){confirm("Do you want to delete this category ?")&&(await axiosInstance.delete(`teacher/${e}`),getCategories())}selectt.addEventListener("change",function(){let e=this.value;nameOrder=e,getCategories()}),getCategories(),categoryForm.addEventListener("submit",function(e){e.preventDefault();let a=fomrElements.firstName.value,t=fomrElements.avatar.value,l=fomrElements.category_phone.value,s=fomrElements.category_email.value,n=fomrElements.category_group.value.split(/\s+/).join(", "),i=categoryForm.elements.category_married.value,o={firstName:a,avatar:t,phoneNumber:l,email:s,isMarried:i,groups:n};null===selected?axios.post(ENDPOINT+"teacher",o).then(e=>{bootstrap.Modal.getInstance(TeacherModal).hide(),getCategories()}):axios.put(ENDPOINT+`teacher/${selected}`,o).then(e=>{bootstrap.Modal.getInstance(TeacherModal).hide(),getCategories()})}),addBtn.addEventListener("click",function(){selected=null,modalBtn.innerHTML="Add Category",categoryForm.reset()}),searchInput.addEventListener("keyup",function(){search=this.value,getCategories()});